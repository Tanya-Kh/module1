﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Module1 module = new Module1();
            int[] testArray = { 0, 1, 2 };

            module.SwapItems(4, 7);
            Console.WriteLine(module.GetMinimumValue(testArray));
        }

        public int[] SwapItems(int a, int b)
        {
            int[] array = { b, a };
            return array;
        }

        public int GetMinimumValue(int[] input)
        {
            int min = input[0];

            for (int i = 1; i < input.Length - 1; i++)
            {
                if (input[i] < min)
                {
                    min = input[i];
                }
            }
            return min;
        }
    }

}
